<?php
    // Template Name: Home page
?>

<?php  get_header( ); ?> 
<section id="principal">
         
         <div class="jumbotron" id="intro">
             <h2 class="display-3"> Doce Confeitaria</h2>
             <h3 class="lead">UMA CONFEITARIA COMPLETA PARA VOCÊ</h3>
         </div>
 
         <div class="p-2 mx-auto" id="descDiv">            
             <div class="p-5 font-italic text-center mw-50">
                 <p>
                     A Lediane Doces nasceu do desejo de fazer Bolos e Cupcakes <br /> diferenciados e com muito carinho.
                 </p>
             </div>
         </div>
         
         <div id="carouselDiv" class="mx-auto w-50" style="background-color: black;">
             <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                 <ol class="carousel-indicators">
                     <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                     <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                     <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                     <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
                 </ol>
             
             <div class="carousel-inner">
                 <div class="carousel-item active">
                     <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/gallery/gallery1.jpg" class="d-block w-100" alt="...">
                         <div class="carousel-caption d-none d-md-block">                  
                             <a href="/">
                                 <div class="text-center btn btn-info">VER +</div>
                             </a>
                         </div>
                 </div>
                 <div class="carousel-item">
                     <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/gallery/gallery2.jpg"  class="d-block w-100" alt="...">
                         <div class="carousel-caption d-none d-md-block">
                             <a href="/">
                                 <div class="btn btn-info">VER +</div>
                             </a>
                         </div>
                 </div>
                 <div class="carousel-item">
                     <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/gallery/gallery3.jpg"  class="d-block w-100" alt="...">
                         <div class="carousel-caption d-none d-md-block">
                             <a href="/">
                                 <div class="btn btn-info">VER +</div>
                             </a>
                         </div>
                 </div>
         
                 <div class="carousel-item">
                     <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/gallery/gallery4.jpg" class="d-block w-100" alt="...">
                         <div class="carousel-caption d-none d-md-block">
                             <a href="/">
                                 <div class="btn btn-info">VER +</div>
                             </a>
                         </div>
                 </div>
             </div>
         
             <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                 <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                 <span class="sr-only">Previous</span>
             </a>
             <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                 <span class="carousel-control-next-icon" aria-hidden="true"></span>
                 <span class="sr-only">Next</span>
             </a>
             </div>
         </div>
 
 
 
 
             <div id="newscontent">
                 <div id="newsletter">
 
                     <div id="newsText">
                         <h2>Nossa página de notícias</h2>
                         <h4>Confira a última notícia</h4>
                     </div>
                     <div>
                         <a href="/">
                             <div class="button">+ NOTICIAS </div>
                         </a>
                     </div>                    
                 </div>
                 </div>
             </div>
 
             
 
         </div>
 
 
     </section>
     
 </body>
 <?php get_footer( ); ?>   