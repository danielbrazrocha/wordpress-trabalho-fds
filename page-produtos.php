<?php
    // Template Name: Page Produtos
?>

<?php  get_header( ); ?> 
          <div class="container">

            <div class="row">
              <div class="album-item col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <a href="#" onclick="openPhotoSwipe(0)">
                  <img  src="<?php echo get_stylesheet_directory_uri(); ?>/img/gallery/gallery1.jpg" alt="Album screenshot" class="img-fluid" width="170" height="245" loading="lazy">
                  </a>     
              </div>
            
              <div class="album-item col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <a href="#" onclick="openPhotoSwipe(1)">
                  <img  src="<?php echo get_stylesheet_directory_uri(); ?>/img/gallery/gallery2.jpg" alt="Album screenshot" class="img-fluid" width="170" height="245" loading="lazy">
                  </a>     
              </div>
              <div class="album-item col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <a href="#" onclick="openPhotoSwipe(2)">
                  <img  src="<?php echo get_stylesheet_directory_uri(); ?>/img/gallery/gallery3.jpg" alt="Album screenshot" class="img-fluid" width="170" height="245" loading="lazy">
                  </a>     
              </div>
              <div class="album-item col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <a href="#" onclick="openPhotoSwipe(3)">
                  <img  src="<?php echo get_stylesheet_directory_uri(); ?>/img/gallery/gallery4.jpg" alt="Album screenshot" class="img-fluid" width="170" height="245" loading="lazy">
                  </a>     
              </div>
              <div class="album-item col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <a href="#" onclick="openPhotoSwipe(4)">
                  <img  src="<?php echo get_stylesheet_directory_uri(); ?>/img/gallery/gallery5.jpg" alt="Album screenshot" class="img-fluid" width="170" height="245" loading="lazy">
                  </a>     
              </div>
              <div class="album-item col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <a href="#" onclick="openPhotoSwipe(5)">
                  <img  src="<?php echo get_stylesheet_directory_uri(); ?>/img/gallery/gallery6.jpg" alt="Album screenshot" class="img-fluid" width="170" height="245" loading="lazy">
                  </a>     
              </div>
              <div class="album-item col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <a href="#" onclick="openPhotoSwipe(6)">
                  <img  src="<?php echo get_stylesheet_directory_uri(); ?>/img/gallery/gallery7.jpg" alt="Album screenshot" class="img-fluid" width="170" height="245" loading="lazy">
                  </a>     
              </div>
              <div class="album-item col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <a href="#" onclick="openPhotoSwipe(7)">
                  <img  src="<?php echo get_stylesheet_directory_uri(); ?>/img/gallery/gallery8.jpg" alt="Album screenshot" class="img-fluid" width="170" height="245" loading="lazy">
                  </a>     
              </div>
              <div class="album-item col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <a href="#" onclick="openPhotoSwipe(8)">
                <img  src="<?php echo get_stylesheet_directory_uri(); ?>/img/gallery/gallery9.jpg" alt="Album screenshot" class="img-fluid" width="170" height="245" loading="lazy">
                  </a>     
              </div>
      
              <div class="album-item col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <a href="#" onclick="openPhotoSwipe(9)">
                <img  src="<?php echo get_stylesheet_directory_uri(); ?>/img/gallery/gallery10.jpg" alt="Album screenshot" class="img-fluid" width="170" height="245" loading="lazy">
                  </a>     
              </div>
              
            </div>
          </div>
      
      
              <!-- Root element of PhotoSwipe. Must have class pswp. -->
      <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
      
        <!-- Background of PhotoSwipe. 
             It's a separate element as animating opacity is faster than rgba(). -->
        <div class="pswp__bg"></div>
      
        <!-- Slides wrapper with overflow:hidden. -->
        <div class="pswp__scroll-wrap">
      
            <!-- Container that holds slides. 
                PhotoSwipe keeps only 3 of them in the DOM to save memory.
                Don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>
      
            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">
      
                <div class="pswp__top-bar">
      
                    <!--  Controls are self-explanatory. Order can be changed. -->
      
                    <div class="pswp__counter"></div>
      
                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
      
                    <button class="pswp__button pswp__button--share" title="Share"></button>
      
                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
      
                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
      
                    <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                    <!-- element will get class pswp__preloader--active when preloader is running -->
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                          <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                          </div>
                        </div>
                    </div>
                </div>
      
                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div> 
                </div>
      
                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                </button>
      
                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                </button>
      
                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>
      
            </div>
      
        </div>          



    </section>
    <?php get_footer( ); ?>  