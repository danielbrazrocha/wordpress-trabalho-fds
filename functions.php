<?php

function confeitaria_scripts() {
    wp_enqueue_style( "reset-sheet", get_stylesheet_directory_uri(  ) . "/style.css");
    wp_enqueue_style( "photoswipe-style-sheet", get_stylesheet_directory_uri() . "/css/default-skin/style.css");
    wp_enqueue_style( "style-sheet", get_stylesheet_directory_uri() . "/css/default-skin/style.css");
	
}
add_action( 'wp_enqueue_scripts', 'confeitaria_scripts' );
?>