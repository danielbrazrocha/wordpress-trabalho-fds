<?php
    // Template Name: Page Sobre
?>

<?php  get_header( ); ?> 
    <section id="principal">
        
        <div class="mt200 text-center m-auto">
            <a href="/">
                <figure>
                    <img class="m=auto" id="sobre" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sobre.jpg" alt="Doce Confeitaria Logo">
                </figure>
            </a>
        </div>

        <div class="p-2 mx-auto" id="descDiv">            
            <div class="p-5 font-italic text-center mw-50">
                <p class="p-3">A Lediane Doces nasceu do desejo de fazer Bolos e Cupcakes com <strong>carinho</strong>, fazendo produtos diferenciados.</p>

                <p class="p-3">Acreditamos que todos deveriam comemorar ocasiões <strong>especiais</strong> com produtos diferenciados!</p>
      
                <p class="p-3">Atendemos em Resende-RJ. Para outras cidades da região, verifique as condições de entrega.</p>
                
            </div>
        </div>
        
        <div id="social_media" class="text-center mx-auto">  
            <div id="socialIconsBar" class="mx-auto">
                <ul class="">          
                    <li>
                        <a href="https://instagram.com/lediane.doce"><i class="fab fa-instagram" style="color:#527fa6;"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/lediane.doce/"><i class="fab fa-facebook" style="color:#3b5a9b;"></i></a>
                    </li>
                    <li>
                        <a href="https://wa.me/5524981010316"><i class="fab fa-whatsapp" style="color:#4AC959;"></i></a>
                    </li>            
                </ul>
            </div>   
            <div>
                <p>Resende - Rio de Janeiro - Brasil</p>
                <p>(24) 98101-0316</p>              
            </div>   
          </div>





    </section>
    <?php get_footer( ); ?>  