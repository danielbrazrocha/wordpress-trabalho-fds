<footer>
  <div id="footer_content">
            <div id="footer_box">
                <div id="footer_text">
                    <p>Este é um projeto da IN Junior</p>
                    <p>www.injunior.com.br</p>
                </div id="footer_img">
                <a href="/">
                <figure>
                <img id="logo_footer" src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner_text.png" class="w-10" alt="Doce Confeitaria Logo">
                </figure>
                </a>
            </div>
        </div>
</footer>

<?php wp_footer(  ) ?>

</body>

<script src="./index.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>



</html>