<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" href="./img/logo.jpg" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
    />
    <link
      href="https://fonts.googleapis.com/css2?family=Felipa&display=swap"
      rel="stylesheet"
    />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(  ) ?>./style.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php wp_head(  ) ?>
    <title>Doce Confeitaria</title>
  </head>
  <body>
    <header id="top">
        <nav class="navbar navbar-expand-lg navbar-dark bg-black">  
          <a href="/">         
            <figure>
                <img id="logo_header" src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner_text.png" alt="Doce Confeitaria Logo">
            </figure>
          </a>

            
           
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <nav class="navbar-nav mr-auto mt-2 mt-lg-0 menuitem">
                <?php
                    $args = array(
                        'menu' => 'principal',
                        'theme_location' => 'menuprinc',
                        'container' => false        
                    );
                    wp_nav_menu( $args );
                ?>
                </nav>

              <!-- <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                  <a class="nav-link" href="/sobre.html">Sobre <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/contato.html">Contato</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/produtos.html">Produtos</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/noticias.html">Noticias</a>
                  </li>
              </ul> -->
              <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Procurar">
                <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Busca</button>
              </form>
            </div>
          </nav>
        
    </header>